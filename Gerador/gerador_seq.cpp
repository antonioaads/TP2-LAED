 /*
                                  /   \       
 _                        )      ((   ))     (
(@)                      /|\      ))_((     /|\
|-|                     / | \    (/\|/\)   / | \                      (@)
| | -------------------/--|-voV---\`|'/--Vov-|--\---------------------|-|
|-|                         '^`   (o o)  '^`                          | |
| |                               `\Y/'                               |-|
|-|      CENTRO FEDERAL DE EDUCAÇÃO TECNOLÓGICA DE MINAS GERAIS       | |
| |          LABORATÓRIO DE ALGORÍTMOS E ESTRUTURA DE DADOS           |-|
|-|                                                                   | |
| |                        TRANALHO PRÁTICO 2                         |-|
|-|                              GERADOR                              | |
| |                           LINGUAGEM C++                           |-|
|-|                                                                   | |
| |            ANTÔNIO AUGUSTO DINIZ SOUSA - 201712040146             |-|
|-|                RODRIGO DIAS MOREIRA - 201712040456                | |
|_|___________________________________________________________________|-|
(@)              l   /\ /         ( (       \ /\   l                `\| |
                 l /   V           \ \       V   \ l                  (@)
                 l/                _) )_          \I
                                   `\ /'
*/

#include <bits/stdc++.h>

using namespace std;

int main()
{
	FILE* file;
	file = fopen("caso_teste.txt","w");

	int size,seq;

	cout << "Gerador de sequencias\n\nTamanho da sequência: ";	
	cin >> size;
	cout << "Selecione a sequencia:\n\t1) Ordenada\n\t2) Inversamente Ordenada\n\t3) Quase Ordenada\n\t4) Aleatória\n> ";
	cin >> seq;

	switch(seq)
	{
		case 1:
		{
			for(int i=0; i<size; i++)
				fprintf(file, "%d\n", i);
		}
		break;
		
		case 2:
		{
			for(int i=size-1; i>=0; i--)
				fprintf(file, "%d\n", i);	
		}
		break;
		
		case 3:
		{
			float desordem;

			cout <<	"Percentual de desordem (0 à 1): ";
			cin >> desordem;
		
			vector<int> v(size);

			// Preencher vetor com sequencia crescente
			for(int i=0; i<v.size(); i++)
				v[i] = i;

			set<int> id_usado; // set que guarda quais indices já foram operados (trocados)

			// "A cada dois trocados, dois elementos estão errados, logo, de 10, ao trocar 2, há 20% de desordem" (mínimo 1 par trocado)
			cout << "it_desordi=" << desordem*size/2+1 << endl;
			for(int i=desordem*size/2+1; i>0; i--)
			{
				int i1;
				do
				{
					i1 = rand()%size;	
				}while(id_usado.find(i1) != id_usado.end()); // enquanto já existir, buscar novo valor
				id_usado.insert(i1); // atualizar lista de ids já utilizados

				int i2;
				do
				{
					i2 = rand()%size;	
				}while(id_usado.find(i2) != id_usado.end()); // enquanto já existir, buscar novo valor
				id_usado.insert(i2); // atualizar lista de ids já utilizados

				int aux = v[i1];
				v[i1] = v[i2];
				v[i2] = aux;
			}

			// Salvar no arquivo
			for(int i=0; i<v.size(); i++)
				fprintf(file, "%d\n", v[i]);
		}
		break;

		case 4:
		{
			set<int> id_usado; // set que guarda quais indices já foram utilizados
		
			for(int i=0; i<size; i++)
			{
				int n;
				do
				{
					n = rand()%size;	
				}while(id_usado.find(n) != id_usado.end()); // enquanto já existir, buscar novo valor
				id_usado.insert(n); // atualizar lista de ids já utilizados

				fprintf(file, "%d\n", n);
			}
		}
		break;
	}


	fclose(file);
	return 0;
}
