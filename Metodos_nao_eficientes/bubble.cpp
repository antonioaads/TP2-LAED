 /*
                                  /   \       
 _                        )      ((   ))     (
(@)                      /|\      ))_((     /|\
|-|                     / | \    (/\|/\)   / | \                      (@)
| | -------------------/--|-voV---\`|'/--Vov-|--\---------------------|-|
|-|                         '^`   (o o)  '^`                          | |
| |                               `\Y/'                               |-|
|-|      CENTRO FEDERAL DE EDUCAÇÃO TECNOLÓGICA DE MINAS GERAIS       | |
| |          LABORATÓRIO DE ALGORÍTMOS E ESTRUTURA DE DADOS           |-|
|-|                                                                   | |
| |                        TRANALHO PRÁTICO 2                         |-|
|-|                       APLICAÇÃO BUBBLESORT                        | |
| |                           LINGUAGEM C++                           |-|
|-|                                                                   | |
| |            ANTÔNIO AUGUSTO DINIZ SOUSA - 201712040146             |-|
|-|                RODRIGO DIAS MOREIRA - 201712040456                | |
|_|___________________________________________________________________|-|
(@)              l   /\ /         ( (       \ /\   l                `\| |
                 l /   V           \ \       V   \ l                  (@)
                 l/                _) )_          \I
                                   `\ /'
*/

#include <bits/stdc++.h>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;

typedef struct Item{
	int key;
	string name;
}Item;

// Ziviani
void Bubblesort(vector<Item> &A)
{
	int n = A.size();

	for(int j=0; j<n-1;j++)
	{
		for(int i=0; i<n-1; i++)
		{
			if(A[i].key > A[i+1].key)
			{
				Item aux = A[i];
				A[i] = A[i+1];
				A[i+1] = aux;
			}
		}
	}
}

// Printar vector
void print(vector<Item> &v)
{
	for(int i=0; i<v.size()-1; i++)
		cout << v[i].key << " - ";
	cout << v[v.size()-1].key << endl;
}

int main()
{
	// Abrir arquivo
	ifstream input("caso_teste.txt");

	vector<Item> v;

	// Preencher vector com elementos
	while(1)
	{
		int i;
		
		input >> i;

		if(input.eof())
			break;
		
		v.push_back((Item){i,"nome"});
	}

	// Visualizar vetor original
	print(v);
	// Executar bubblesort
	Bubblesort(v);
	// Visualizar vetor final
	print(v);

	input.close();


	// Tempo
		struct rusage usage;
		
		int who = RUSAGE_SELF;  //man: information shall be returned about resources used by the current process

		long utotalmicroseg, utotalseg;
		long stotalmicroseg, stotalseg;

		getrusage(who, &usage);

		 //tempo de usuário na CPU
		 utotalseg = usage.ru_utime.tv_sec; //segundos
		 utotalmicroseg = usage.ru_utime.tv_usec; //microsegundos

		 //tempo de sistema na CPU
		 stotalseg = usage.ru_stime.tv_sec; //segundos
		 stotalmicroseg = usage.ru_stime.tv_usec; //microsegundos
		 printf ("\n");
		 printf ("***************************************************************\n");
		 printf ("Tempo de usuario: %ld segundos e %ld microssegundos.\n", utotalseg, utotalmicroseg);
		 printf ("Tempo de sistema: %ld segundos e %ld microssegundos.\n", stotalseg, stotalmicroseg);
		 printf ("***************************************************************\n");
		 printf ("\n");

	return 0;
}