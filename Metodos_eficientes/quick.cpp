 /*
                                  /   \       
 _                        )      ((   ))     (
(@)                      /|\      ))_((     /|\
|-|                     / | \    (/\|/\)   / | \                      (@)
| | -------------------/--|-voV---\`|'/--Vov-|--\---------------------|-|
|-|                         '^`   (o o)  '^`                          | |
| |                               `\Y/'                               |-|
|-|      CENTRO FEDERAL DE EDUCAÇÃO TECNOLÓGICA DE MINAS GERAIS       | |
| |          LABORATÓRIO DE ALGORÍTMOS E ESTRUTURA DE DADOS           |-|
|-|                                                                   | |
| |                        TRANALHO PRÁTICO 2                         |-|
|-|                        APLICAÇÃO QUICKSORT                        | |
| |                           LINGUAGEM C++                           |-|
|-|                                                                   | |
| |            ANTÔNIO AUGUSTO DINIZ SOUSA - 201712040146             |-|
|-|                RODRIGO DIAS MOREIRA - 201712040456                | |
|_|___________________________________________________________________|-|
(@)              l   /\ /         ( (       \ /\   l                `\| |
                 l /   V           \ \       V   \ l                  (@)
                 l/                _) )_          \I
                                   `\ /'
*/

#include <bits/stdc++.h>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;
#define MAXTAM 10000000

typedef long TipoChave;
typedef struct TipoItem {
  TipoChave Chave;
  /* outros componentes */
  char Nome[100];
} TipoItem;

typedef int TipoIndice;
typedef TipoItem TipoVetor[MAXTAM + 1]; 
/* MAXTAM+1 por causa da sentinela em Insercao */

// Ziviani
void Particao(TipoIndice Esq, TipoIndice Dir, 
              TipoIndice *i, TipoIndice *j, TipoItem *A)
{ TipoItem x, w;
  *i = Esq;  *j = Dir;
  x = A[(*i + *j) / 2]; /* obtem o pivo x */
  do 
    { while (x.Chave > A[*i].Chave) (*i)++;
      while (x.Chave < A[*j].Chave) (*j)--;
      if (*i <= *j) 
      { w = A[*i]; A[*i] = A[*j]; A[*j] = w;
        (*i)++; (*j)--;
      }
    } while (*i <= *j);
}

void Ordena(TipoIndice Esq, TipoIndice Dir, TipoItem *A)
{ TipoIndice i, j;
  Particao(Esq, Dir, &i, &j, A);
  if (Esq < j) Ordena(Esq, j, A);
  if (i < Dir) Ordena(i, Dir, A);
}

void QuickSort(TipoItem *A, TipoIndice n)
{ Ordena(1, n, A); }

// Printar vector
void print(TipoItem* v,int n)
{
  for(int i=0; i<n-1; i++)
    cout << v[i].Chave << " - ";
  cout << v[n-1].Chave << endl;
}

int main()
{
  // Abrir arquivo
  ifstream input("caso_teste.txt");

  TipoItem* v = (TipoItem*)malloc(MAXTAM*sizeof(TipoItem));
  v[0]=(TipoItem){-1,"sentinela"}; // "inutiliza" posição 0, como pressuposto por Ziviani (utilizado como sentinela)

  // Preencher vector com elementos
  int n=1;
  while(1)
  {
    int id; 
    input >> id;

    if(input.eof())
      break;
    
    v[n]=(TipoItem){id,"Nome"};
    
    n++;
  }

  // Visualizar vetor original
  print(v,n);
  // Executar bubblesort
  QuickSort(v,n-1);
  // Visualizar vetor final
  print(v,n);

  input.close();

  // Tempo
    struct rusage usage;
    
    int who = RUSAGE_SELF;  //man: information shall be returned about resources used by the current process

    long utotalmicroseg, utotalseg;
    long stotalmicroseg, stotalseg;

    getrusage(who, &usage);

     //tempo de usuário na CPU
     utotalseg = usage.ru_utime.tv_sec; //segundos
     utotalmicroseg = usage.ru_utime.tv_usec; //microsegundos

     //tempo de sistema na CPU
     stotalseg = usage.ru_stime.tv_sec; //segundos
     stotalmicroseg = usage.ru_stime.tv_usec; //microsegundos
     printf ("\n");
     printf ("***************************************************************\n");
     printf ("Tempo de usuario: %ld segundos e %ld microssegundos.\n", utotalseg, utotalmicroseg);
     printf ("Tempo de sistema: %ld segundos e %ld microssegundos.\n", stotalseg, stotalmicroseg);
     printf ("***************************************************************\n");
     printf ("\n");

  return 0;
}