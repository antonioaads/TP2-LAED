  /*
                                  /   \       
 _                        )      ((   ))     (
(@)                      /|\      ))_((     /|\
|-|                     / | \    (/\|/\)   / | \                      (@)
| | -------------------/--|-voV---\`|'/--Vov-|--\---------------------|-|
|-|                         '^`   (o o)  '^`                          | |
| |                               `\Y/'                               |-|
|-|      CENTRO FEDERAL DE EDUCAÇÃO TECNOLÓGICA DE MINAS GERAIS       | |
| |          LABORATÓRIO DE ALGORÍTMOS E ESTRUTURA DE DADOS           |-|
|-|                                                                   | |
| |                        TRANALHO PRÁTICO 2                         |-|
|-|                        APLICAÇÃO MERGESORT                        | |
| |                           LINGUAGEM C++                           |-|
|-|                                                                   | |
| |            ANTÔNIO AUGUSTO DINIZ SOUSA - 201712040146             |-|
|-|                RODRIGO DIAS MOREIRA - 201712040456                | |
|_|___________________________________________________________________|-|
(@)              l   /\ /         ( (       \ /\   l                `\| |
                 l /   V           \ \       V   \ l                  (@)
                 l/                _) )_          \I
                                   `\ /'
*/

#include <bits/stdc++.h>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;
#define MAXTAM 10000000

typedef long TipoChave;
typedef struct TipoItem {
  TipoChave Chave;
  /* outros componentes */
  char Nome[100];
} TipoItem;

typedef int TipoIndice;
typedef TipoItem TipoVetor[MAXTAM + 1]; 
/* MAXTAM+1 por causa da sentinela em Insercao */

// Ziviani
void Merge(TipoItem* A,int i,int m,int j)
{
  TipoItem* B = (TipoItem*)malloc(MAXTAM*sizeof(TipoItem));
  int x;
  int k = i;
  int l = m+1;

  for(x=i; x<=j; x++)
    B[x] = A[x];

  x = i;

  while(k<=m && l<=j)
  {
    if(B[k].Chave<=B[l].Chave)
      A[x++]=B[k++];
    else
      A[x++]=B[l++];
  }

  while(k<=m)
    A[x++]=B[k++];
  while(l<=j)
    A[x++]=B[l++];

}

void Mergesort(TipoItem* A, int i, int j)
{
  int m;
  if(i<j)
  {
    m = (i+j-1)/2;
    Mergesort(A,i,m);
    Mergesort(A,m+1,j);
    Merge(A,i,m,j);
  }
}

// Printar vector
void print(TipoItem* v,int n)
{
  for(int i=0; i<n-1; i++)
    cout << v[i].Chave << " - ";
  cout << v[n-1].Chave << endl;
}

int main()
{
  // Abrir arquivo
  ifstream input("caso_teste.txt");

  TipoItem* v = (TipoItem*)malloc(MAXTAM*sizeof(TipoItem));
  v[0]=(TipoItem){-1,"sentinela"}; // "inutiliza" posição 0, como pressuposto por Ziviani (utilizado como sentinela)

  // Preencher vector com elementos
  int n=1;
  while(1)
  {
    int id; 
    input >> id;

    if(input.eof())
      break;
    
    v[n]=(TipoItem){id,"Nome"};
    
    n++;
  }

  // Visualizar vetor original
  print(v,n);
  // Executar bubblesort
  Mergesort(v,0,n-1);
  // Visualizar vetor final
  print(v,n);

  input.close();

  // Tempo
    struct rusage usage;
    
    int who = RUSAGE_SELF;  //man: information shall be returned about resources used by the current process

    long utotalmicroseg, utotalseg;
    long stotalmicroseg, stotalseg;

    getrusage(who, &usage);

     //tempo de usuário na CPU
     utotalseg = usage.ru_utime.tv_sec; //segundos
     utotalmicroseg = usage.ru_utime.tv_usec; //microsegundos

     //tempo de sistema na CPU
     stotalseg = usage.ru_stime.tv_sec; //segundos
     stotalmicroseg = usage.ru_stime.tv_usec; //microsegundos
     printf ("\n");
     printf ("***************************************************************\n");
     printf ("Tempo de usuario: %ld segundos e %ld microssegundos.\n", utotalseg, utotalmicroseg);
     printf ("Tempo de sistema: %ld segundos e %ld microssegundos.\n", stotalseg, stotalmicroseg);
     printf ("***************************************************************\n");
     printf ("\n");

  return 0;
}